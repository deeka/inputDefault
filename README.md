inputDefault
===================================
InputDefault 是一个jQuery 插件，用来实现对文本输入框的默认值的自动切换，当鼠标移开时自动显示默认值，当输入框获取输入焦点时，删除默认值。其作用就不作详细描述了，直接看演示。

作者
-----------------------------------
[喵了个咪](http://www.hdj.me)


版本
-----------------------------------
V1.0

参数
-----------------------------------
名称	默认值	说明  
attrName	fs	默认属性  
size	0	字体大小，默认为input字体大小  
bold	false	粗体  
italic	false	斜体  
color	#CCC	字体颜色  

用法
-----------------------------------
```javascript
$('[fs]').inputDefault();
```

演示
-----------------------------------
[http://www.hdj.me/demos/inputdefault/index.html](http://www.hdj.me/demos/inputdefault/index.html)